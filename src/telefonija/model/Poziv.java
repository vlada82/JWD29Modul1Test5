package telefonija.model;

import java.sql.Date;

public class Poziv {
	
	protected int id;
	protected Date pocetak;
	protected Date zavrsetak;
	protected KorisnikNalog pozivalac;
	protected KorisnikNalog primalac;
	
	
	public Poziv() {
		
	}


	public Poziv(int id, Date pocetak, Date zavrsetak) {
		super();
		this.id = id;
		this.pocetak = pocetak;
		this.zavrsetak = zavrsetak;
	}


	public Poziv(int id, Date pocetak, Date zavrsetak, KorisnikNalog pozivalac, KorisnikNalog primalac) {
		super();
		this.id = id;
		this.pocetak = pocetak;
		this.zavrsetak = zavrsetak;
		this.pozivalac = pozivalac;
		this.primalac = primalac;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Date getPocetak() {
		return pocetak;
	}


	public void setPocetak(Date pocetak) {
		this.pocetak = pocetak;
	}


	public Date getZavrsetak() {
		return zavrsetak;
	}


	public void setZavrsetak(Date zavrsetak) {
		this.zavrsetak = zavrsetak;
	}


	public KorisnikNalog getPozivalac() {
		return pozivalac;
	}


	public void setPozivalac(KorisnikNalog pozivalac) {
		this.pozivalac = pozivalac;
	}


	public KorisnikNalog getPrimalac() {
		return primalac;
	}


	public void setPrimalac(KorisnikNalog primalac) {
		this.primalac = primalac;
	}


	@Override
	public String toString() {
		return "Poziv [id=" + id + ", pocetak=" + pocetak + ", zavrsetak=" + zavrsetak + ", pozivalac=" + pozivalac
				+ ", primalac=" + primalac + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((pozivalac == null) ? 0 : pozivalac.hashCode());
		result = prime * result + ((primalac == null) ? 0 : primalac.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Poziv other = (Poziv) obj;
		if (id != other.id)
			return false;
		if (pozivalac == null) {
			if (other.pozivalac != null)
				return false;
		} else if (!pozivalac.equals(other.pozivalac))
			return false;
		if (primalac == null) {
			if (other.primalac != null)
				return false;
		} else if (!primalac.equals(other.primalac))
			return false;
		return true;
	}
	
	

}
