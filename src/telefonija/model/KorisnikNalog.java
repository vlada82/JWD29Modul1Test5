package telefonija.model;

import java.util.ArrayList;

public class KorisnikNalog {
	
	protected int id;
	protected int sifra;
	protected String brTel;
	protected int freeMin; //minuti se zaokuruzuju na 60sec
	protected double stanje;
	
	protected
	ArrayList<Poziv> pozivi;
	
	public KorisnikNalog() {
		
	}


	public KorisnikNalog(int id, int sifra, String brTel, int freeMin, double stanje) {
		this.id = id;
		this.sifra = sifra;
		this.brTel = brTel;
		this.freeMin = freeMin;
		this.stanje = stanje;
	}


	public KorisnikNalog(int id, int sifra, String brTel, int freeMin, double stanje, ArrayList<Poziv> pozivi) {
		super();
		this.id = id;
		this.sifra = sifra;
		this.brTel = brTel;
		this.freeMin = freeMin;
		this.stanje = stanje;
		this.pozivi = pozivi;
	}
	
	


	public KorisnikNalog(int sifra, String brTel) {
		super();
		this.sifra = sifra;
		this.brTel = brTel;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getSifra() {
		return sifra;
	}


	public void setSifra(int sifra) {
		this.sifra = sifra;
	}


	public String getBrTel() {
		return brTel;
	}


	public void setBrTel(String brTel) {
		this.brTel = brTel;
	}


	public int getFreeMin() {
		return freeMin;
	}


	public void setFreeMin(int freeMin) {
		this.freeMin = freeMin;
	}


	public double getStanje() {
		return stanje;
	}


	public void setStanje(double stanje) {
		this.stanje = stanje;
	}


	public ArrayList<Poziv> getPozivi() {
		return pozivi;
	}


	public void setPozivi(ArrayList<Poziv> pozivi) {
		this.pozivi = pozivi;
	}


	@Override
	public String toString() {
		return "KorisnikNalog [id=" + id + ", sifra=" + sifra + ", brTel=" + brTel + ", freeMin=" + freeMin
				+ ", stanje=" + stanje + ", pozivi=" + pozivi + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brTel == null) ? 0 : brTel.hashCode());
		result = prime * result + sifra;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KorisnikNalog other = (KorisnikNalog) obj;
		if (brTel == null) {
			if (other.brTel != null)
				return false;
		} else if (!brTel.equals(other.brTel))
			return false;
		if (sifra != other.sifra)
			return false;
		return true;
	}

	
	
}
