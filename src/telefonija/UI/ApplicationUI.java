package telefonija.UI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import telefonija.Utils.PomocnaKlasa;

public class ApplicationUI {

	public static void main(String[] args) {

		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/mobtelefonija?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			System.exit(0);
		}

		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
				case 0:
					System.out.println("Izlaz iz programa");
					break;
				case 1:
					KorisnikNalogUI.unos(conn);
					break;
				case 2:
					//PozivUI.prikazPoNalogu(conn);
					break;
				case 3:
					//PozivUI.unos(conn);
					break;
				case 4:
					KorisnikNalogUI.ispisSve(conn);
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;
			}
		}
		
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public static void ispisiMenu() {
		System.out.println("Mobilna Telefonija - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - unos novog naloga");
		System.out.println("\tOpcija broj 2 - prikaz naloga sa svim pozivima");
		System.out.println("\tOpcija broj 3 - dodavanje poziva");
		System.out.println("\tOpcija broj 4 - ispis svih naloga");

		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

		
	

}
