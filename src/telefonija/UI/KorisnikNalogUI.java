package telefonija.UI;

import java.sql.Connection;
import java.util.List;
import telefonija.DAO.KorisnikNalogDAO;
import telefonija.Utils.PomocnaKlasa;
import telefonija.model.KorisnikNalog;

public class KorisnikNalogUI {

	public static void unos(Connection conn) {
		
		System.out.println("Unesite jedinstvenu sifru korisnika : ");
		int sifra = PomocnaKlasa.ocitajCeoBroj();
		
		System.out.println("Unesite broj telefona korisnika : ");
		String brTel = PomocnaKlasa.ocitajTekst();
		
		//int freeMin = 100; //korisnici po default-u dobijajau 100min
		
//		System.out.println("Unesite jedinstvenu sifru korisnika : ");
//		int sifra = PomocnaKlasa.ocitajCeoBroj();

		
		KorisnikNalog korisnik = new KorisnikNalog(sifra, brTel);
		KorisnikNalogDAO.add(conn, korisnik);

		
	}

	public static void ispisSve(Connection conn) {

		List<KorisnikNalog> nalozi = KorisnikNalogDAO.getAll(conn);
		for (KorisnikNalog itNalog: nalozi)
			System.out.println(itNalog);
		
	}

}
