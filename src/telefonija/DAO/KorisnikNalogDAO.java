package telefonija.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import telefonija.model.KorisnikNalog;

public class KorisnikNalogDAO {

	public static boolean add(Connection conn, KorisnikNalog korisnik) {

		PreparedStatement pstmt = null;
		try {
			conn.setAutoCommit(false);
			conn.commit();
			
			String query = "INSERT INTO nalog (sifra, brojTelefona) VALUES (?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, korisnik.getSifra());
			pstmt.setString (index++, korisnik.getBrTel());
		
			pstmt.executeUpdate();
			
//			RacunDAO.updateRaspolozivoStanje(conn, nalog.getUplatilac());
//			conn.commit(); // kraj transakcije
			return true;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
			try {conn.rollback();} catch (SQLException ex1) { ex1.printStackTrace(); } // vratiti bazu u stanje pre pocetka transakcije
			return false;
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
	}

	public static List<KorisnikNalog> getAll(Connection conn) {
		List<KorisnikNalog> nalozi = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, sifra, brojTelefona, freeMinuti, stanje FROM nalog";

			stmt = conn.createStatement();

			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int sifra = rset.getInt(index++);
				String brTel = rset.getString(index++);
				int freeM = rset.getInt(index++);
				double stanje = rset.getDouble(index++);


				KorisnikNalog nalog = new KorisnikNalog(id, sifra, brTel, freeM, stanje);
				nalozi.add(nalog);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return nalozi;
	}

}
