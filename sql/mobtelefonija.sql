DROP SCHEMA IF EXISTS mobtelefonija;
CREATE SCHEMA mobtelefonija DEFAULT CHARACTER SET utf8;
USE mobtelefonija;

CREATE TABLE nalog (
	id INT AUTO_INCREMENT,
	sifra INT NOT NULL,
	brojTelefona VARCHAR(15) NOT NULL,
	freeMinuti INT NOT NULL,
	stanje DECIMAL (10,2) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE poziv (
	id INT AUTO_INCREMENT,
	pocetak DATETIME NOT NULL,
	zavrsetak DATETIME NOT NULL,
	idPozivaoca INT NOT NULL,
	idPrimaoca INT NOT NULL, 
	PRIMARY KEY (id),
	
	FOREIGN KEY (idPozivaoca) REFERENCES nalog(id),
	FOREIGN KEY (idPrimaoca) REFERENCES nalog(id)
);

INSERT INTO nalog (id, sifra, brojTelefona, freeMinuti, stanje) VALUES (0, 001,'0631234567', 2, 200.00);
INSERT INTO nalog (id, sifra, brojTelefona, freeMinuti, stanje) VALUES (0, 001,'0642345678', 350, 13000.00);
INSERT INTO nalog (id, sifra, brojTelefona, freeMinuti, stanje) VALUES (0, 001,'0653456789', 88, 8000.00);
